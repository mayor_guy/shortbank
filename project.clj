(defproject shortbank "0.1.0-SNAPSHOT"
  :description "Connects to banking solutions for transfer purposes"
  :url "http://larntandigital.com.ng"
  :omit-source true
  :license {:name "larntan digital"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [http-kit "2.2.0"]
                 [compojure "1.6.0"]
                 [log4j/log4j "1.2.16" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jdmk/jmxtools
                                                    com.sun.jmx/jmxri]]

                 [org.clojure/tools.logging "0.3.1"]
                 [korma "0.4.3"]
                 [org.postgresql/postgresql "9.4-1200-jdbc41"]
                 [com.mchange/mchange-commons-java "0.2.12"]
                 [org.clojure/data.json "0.2.6"]]

  :profiles  {:uberjar
              {:aot :all}}

  :jvm-opts ["-Dcom.yim.shortbank.file=F:/env/config-transfer.md"]

  :main com.yim.shortbank.service)
