(ns com.yim.shortbank.callbank
  (:use [clojure.tools.logging])
  (:require [org.httpkit.client :as http]
            [clojure.data.json :as json]
            [com.yim.shortbank.rpc :as rpc]
            [com.yim.shortbank.config :as conf])
  (:import (java.net ConnectException SocketTimeoutException)))



(declare openurl)

(defn getBankNames [& args]
  (openurl "getBankNames" conf/bank-api-url args))

(defn getBankBalance [& args]
  (openurl "getBankBalance" conf/bank-balance-url args))


;url "https://api.paystack.co/bank" -H "Authorization: Bearer sk_test_3f7038011f02ef68197ec32d05b2c0a97835af6c" -X GET
(defn openurl [logname url & args]
  (rpc/with-time-logging [logname url args]
                         ;;(.toURI #^java.net.URL (java.net.URL. (format config/psa-url request-id))) request (psa-header)
                         (let [options  {:timeout (Integer.  conf/app-bank-timeout) ; ms
                                         :oauth-token conf/bank-public-key
                                         :accept :json
                                         :content-type :json
                                         :headers {"Content-Type" "application/json"}}
                               {:keys [status body error] :as trace}  @(http/get url options)]
                           (try
                             (if error
                               (let [error-msg (condp instance? true
                                                 org.httpkit.client.TimeoutException ":timeout-on-connect"
                                                 "error")]
            (errorf "Connection exception [%s|%s]" error-msg trace)
                                 [false error])
                               (if (not-empty body)
                                 (let [obj (do
                                             (infof "Body message [%s]" body)
                                             (json/read-str body :key-fn keyword))]
                                   (infof "Parsed response(%s)"obj)
                                   [true obj])
                                 (do
                                   (errorf "Error: Empty body [%s]" trace)
                                   [false " "])))
                             (catch Exception e
                               (errorf "Exception error [%s]" (.getMessage e))
                               [false (.getMessage e)])))))


