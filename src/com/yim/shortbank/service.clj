(ns com.yim.shortbank.service
  (:gen-class :initialize true)
  (:use [clojure.tools.logging]
        [org.httpkit.server]
        [compojure.core]
        [com.yim.shortbank.config]
        [com.yim.shortbank.dbconf])
  (:require
    [com.yim.shortbank.rpc :as rpc]
    [com.yim.shortbank.callIN :as callIN]
    [com.yim.shortbank.callbank :as callbank]
    [clojure.data.json :as json]
    [clojure.string :as str]
    [com.yim.shortbank.dbconf :as db])
  (:import (java.util.concurrent Executors TimeUnit)
           (java.net URLDecoder)))

(declare navigate-menu proceed-menu-1 proceed-menu-2)

(def ^:dynamic *countof-decide-failures* (atom {}))
(def ^:dynamic *countof-decisioning-requests* (atom 0))
(def ^:dynamic *countof-qualifiers* (atom {}))




(defn- decide [subscriber]
  (rpc/with-funcall-logging ["decide" subscriber]
                            (swap! *countof-decisioning-requests* inc)
                            (let [request-id (rpc/request-id)]
                              [request-id
                               (let [sub_profile (decisions subscriber)]
                                 (cond (= sub_profile :unknown-subscriber)
                                       (do (rpc/increment-counter *countof-decide-failures* :unknown-subscriber)
                                           :unknown-subscriber)
                                       (empty? sub_profile)
                                       (do (rpc/increment-counter *countof-decide-failures* :no-profile)
                                           :no-profile)
                                       (= sub_profile "postpaid")
                                       (do (rpc/increment-counter *countof-decide-failures* :postpaid-subscriber)
                                           :postpaid-subscriber)
                                       (= sub_profile "hybrid")
                                       (do (rpc/increment-counter *countof-decide-failures* :hybrid-subscriber)
                                           :hybrid-subscriber)
                                       :else (do (rpc/increment-counter *countof-qualifiers* sub_profile)
                                                 :unknown-subscriber-state)))])))


(defn- do-handle-ussd-request [session-id subscriber is-new? option]
  (let [session-data (if is-new?
                       {:session-id session-id :subscriber subscriber :state :initial}
                       (get-session-data session-id subscriber))]
    (infof "Session data(%s)"session-data)
    (navigate-menu subscriber session-id session-data option)))


(defmulti navigate-menu
          (fn [subscriber session-id session-data option]
            (session-data :state)))


(defmethod navigate-menu :initial [subscriber session-id session-data option]
  ;; First, determine whether or not the subscriber has supplied all
  ;; information required to process their request ie. PIN and amount.e
  (let [[request-id sub_profile] (decide subscriber)
        unreconciled-transfer-request? (> (check-unreconciled-txn subscriber) 0)
        ;; ---
        res= #(= sub_profile %)
        stop #(rpc/make-response 200 :terminate subscriber %)
        start-session (fn [subscriber]
                        (start-new-session {:session-id session-id :state :menu :request-id request-id
                                               :subscriber subscriber})
                        (rpc/make-response 200 :continue subscriber ussd-subscription-top-menu))
        start-session-onqueue (fn [msg-on-queue]
                                (start-new-session {:session-id session-id :state :menu :subscriber subscriber})
                                (rpc/make-response 200 :terminate subscriber msg-on-queue))]
    (cond (res= :unknown-subscriber) (stop msg-qualification-failure)
          (res= :no-profile) (stop msg-qualification-failure)
          (res= :postpaid-subscriber) (stop msg-postpaid)
          (res= :hybrid-subscriber)   (stop msg-postpaid) ;;same message is alright
          unreconciled-transfer-request? (start-session-onqueue msg-has-queued-request )
          (res= :unknown)       (stop msg-decisioning-error)
          :else  (start-session subscriber))))


(defmethod navigate-menu :menu [subscriber session-id session-data option]
  (let [input (biginteger option)
        ;;--------------;;
        ;;borrow now    ;;
        ;;--------------;;
        ; unreconciled-loan-request? (> (check-unreconciled-txn subscriber) 0)
        ]
    (if (number? input)
      (let [[success? result] [true 2000]
            ;temporary disable getMAbal since am on localmachine
            ;(callIN/getMABal session-id subscriber amount)
            ]
        (if success?
          (proceed-menu-1 subscriber session-id session-data input result)
          (rpc/make-response 200 :terminate subscriber msg-error-menu)))
      (rpc/make-response 200 :terminate subscriber msg-retry-menu))))

(defn proceed-menu-1 [subscriber session-id session-data input result]
  (let [maBal (biginteger result)
        proceed? (do
                   (infof "type input %s |%s and type result %s|%s" input (type input) maBal (type maBal))
                   (>= maBal input))]
    (if proceed?
      (let [[success? result](callbank/getBankBalance session-id subscriber input)]
        (if-not success?
          (do
            (infof "Failed Payment Gateway Result =>%s"result)
            (rpc/make-response 200 :terminate subscriber msg-bank-unavailable))
          ;;process bank lists
          (let [{status :status data :data} result]
            (infof "Successful payment gateway result (%s)"result)
            (if status
              (let [[data] data
                    paygwBal (biginteger (:balance data))
                    proceed? (>= paygwBal input)]
                (infof "Input=%s,paygwBal=%s"input paygwBal)
                (if proceed?
                  (proceed-menu-2 subscriber session-id session-data input result)
                  (rpc/make-response 200 :terminate subscriber msg-service-unavailable)))
              (rpc/make-response 200 :terminate subscriber msg-bank-unavailable)))))
      (rpc/make-response 200 :terminate subscriber (format msg-low-balance input maBal)))))

(defn- proceed-menu-2 [subscriber session-id session-data input result]
  (let [[success? result](callbank/getBankNames subscriber session-id input result)]
    (if-not success?
      (do
        (infof "Result =>%s"result)
        (rpc/make-response 200 :terminate subscriber msg-bank-unavailable))
      ;;process bank lists
      (let [{status :status message :message data :data} result]
        (if status
          (for [records (sort-by :id data)
                :let [{name :name active :active id :id
                       code :code longcode :longcode :as all} records]]
            (when active
              [id name code longcode all]))
          (rpc/make-response 200 :terminate subscriber msg-bank-unavailable))))))

  (defn process-request
    "Receives all status check requests, processes them and provides appropriate
    response."
    [req]
    (when req
      (debugf "Status request received [%s]" req)
      (let [{msisdn  :sub
             sidd    :sid
             state   :state
             input   :input} (rpc/process-url-encoded req)]

        (if (= "empty" (rpc/check (list input msisdn sidd state)))
          (do
            (errorf "Content is empty or incorrect [msisdn=%s,sid=%s,state=%s,input=%s]" msisdn sidd state input)
            (rpc/make-response 400 :terminate msisdn "empty content"))

          (try
            (let [state (biginteger state)
                  msisdn (biginteger msisdn)
                  input (when input (URLDecoder/decode input))
                  is-new? (cond (= state 0) true
                                (= state 1) false
                                :else :unknown)]
              (if (= is-new? :unknown)
                (do (error (format "badState(%s,sub=%s,sid=%s,msg=%s)"
                                   state msisdn sidd input))
                    (rpc/client-error))
                (do-handle-ussd-request sidd msisdn is-new? input)))
            (catch Exception e
              (do
                (error "Something bad just happened: " (.getMessage e) "|" e)
                (rpc/make-response 400 :terminate msisdn msg-ex-error))))))))


  (defn check_balance [msisdn sidd state input]
    (infof "Verifying request -> [msisdn=%s,sid=%s,state=%s,input=%s]" msisdn sidd state input)
    (let [msisdn (submsisdn msisdn)
          sid (biginteger sidd)
          input (str/replace input #"\+" " ")
          getBal (do
                   (infof "Get MA balance (sub=%,sid=%,input=%)" msisdn sid input)
                   (callIN/getMABal sid msisdn))]
      (if (nil? getBal) (let [value-split  (str/split getBal #":")
                              value (value-split 1)]
                          (if (neg? (Integer. value))
                            (rpc/make-response 200 :continue msisdn value))
                          (rpc/make-response 200 :terminate msisdn "error")))))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (defonce server (atom nil))

  (defn stop-server []
    (when-not (nil? @server)
      ;; graceful shutdown: wait 100ms for existing requests to be finished
      ;; :timeout is optional, when no timeout, stop immediately
      (@server :timeout 100)
      (reset! server nil)))

  (defroutes all-routes
             (GET "/shortbank/process" [req] process-request)
             (GET "/test" [req] {:status   200
                                 :headers {"Content-Type" "application/json"}
                                 :body    "Author: yilori\n"})
             (ANY "*" [req] {:status   404
                             :headers {"Content-Type" "application/json"}
                             :body    "Page not Found, contact @yilori\n"}))

(defn initialize-app [& args]
  (info "Service Starting...")
  ;(call-get-denoms "start")
  (initialise-app-server-config)
  (db/initialize-db {:db         dbname
                     :user       dbuser
                     :password   dbpassword
                     :host       dbhost
                     :port       dbport
                     :delimiters ""})
  (db/start-partition-creator))

  (defn -main [& args]
    (initialize-app)
    (let [port app-port]
      (reset! server (run-server all-routes {:port port}))
      (infof "Server now accepting requests on port [%d]" port))

    (.addShutdownHook (Runtime/getRuntime)
                      (Thread. (fn [] (info "APPServer shutting down...")
                                 (stop-server)))))

  (-main)