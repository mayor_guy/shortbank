(ns com.yim.shortbank.callsms
  (:use [clojure.xml :only [parse]])
  (:use [clojure.tools.logging])
  (:require [clojure.string :as str]
            [com.yim.shortbank.dbconf :as db]
            [com.yim.shortbank.config :as conf]
            [org.httpkit.client :as http :exclude get]))



(defn callsms
  "Calls SMS URL and return failed or success with corresponding
  status code"
  [msisdn msg]
  (info "callSMS |" msg)
  (let [flag "true" ;(db/check-flag msisdn)
        ]
    (if (empty? flag)
      (let [options  {:socket-timeout (Integer. conf/read-timeout )
                      :conn-timeout (Integer. conf/connect-timeout)
                      :timeout (Integer. conf/connect-timeout)             ; ms
                      :query-params {:to msisdn :text msg :header conf/sms-header}}
            {:keys [status  body error] :as resp}  @(http/get conf/sms-url options)]
        (if error
          (do
            (errorf "!sendSMS failed [%s|%s]" error resp)
            "sms-failed")
          (do
            (infof "sendSMS success [%s|%s|%s|flag=%s]" status msisdn body flag)
            "sms-success" )))
      (do
        (infof "Backend success acknowledge  [%s|%s]" msisdn flag)
        "sms-success" ))))


#_(



    (defn callmention
      "Calls SMS URL and return failed or success with corresponding
      status code"
      []
      (println "callSMS | Debit Request")
      (let [options {:timeout 5000
                     :oauth-token "NDk1MzI4MWUwNTRjZDkxMWQxNDU5ZTU1NjAyZTRlZmE2MjEwNTE2ZTY4ZjcxMWZiMTkwZDA4NzA2MjQxN2FhOA"
                     ;:body {json/encode {"key" "value"}
                     :headers {"Content-Type"  "application/json"
                               "Authorization" "Bearer NDk1MzI4MWUwNTRjZDkxMWQxNDU5ZTU1NjAyZTRlZmE2MjEwNTE2ZTY4ZjcxMWZiMTkwZDA4NzA2MjQxN2FhOA"}
                     }
            {:keys [status  body error] :as resp}  @(http/get "https://api.mention.net/api/accounts/772921_qxho63ya1zk8sk0840sg8cwok84gkogoskw808k4ssosks844/alerts" options)]
        (if error
          (do
            (println "!sendSMS failed" error resp)
            "sms-failed")
          (do
            (println "sendSMS success" status body)
            "sms-success" ))))

    ;https://api.mention.net/api/accounts/{account_id}/alerts/{alert_id}

    (defn createAlert
      "Calls SMS URL and return failed or success with corresponding
      status code"
      [topic]
      (println "Create Alert")
      (let [options {:timeout     5000
                     :oauth-token "NDk1MzI4MWUwNTRjZDkxMWQxNDU5ZTU1NjAyZTRlZmE2MjEwNTE2ZTY4ZjcxMWZiMTkwZDA4NzA2MjQxN2FhOA"
                     :body        (json/encode {"name" topic
                                                "query"  {"type" "basic"
                                                          ;keywords should be a split of
                                                          ;query
                                                          "included_keywords" [topic]}
                                                "languages" ["en"]
                                                "sources"   ["web"]})
                     :headers    {"Content-Type"  "application/json"
                                  "Authorization" "Bearer NDk1MzI4MWUwNTRjZDkxMWQxNDU5ZTU1NjAyZTRlZmE2MjEwNTE2ZTY4ZjcxMWZiMTkwZDA4NzA2MjQxN2FhOA"}
                     }
            {:keys [status  body error] :as resp}  @(http/post "https://api.mention.net/api/accounts/772921_qxho63ya1zk8sk0840sg8cwok84gkogoskw808k4ssosks844/alerts" options)]

        (if (= status 200)
          (do
            (println "create alert success" status body)
            "createalert-success" )
          (do
            (println "!create alert failed" error resp)
            "createalert-failed"))))

    (defn fetchAlert
      [id]
      (println "Create Alert")
      (let [options {:timeout     5000
                     :oauth-token "NDk1MzI4MWUwNTRjZDkxMWQxNDU5ZTU1NjAyZTRlZmE2MjEwNTE2ZTY4ZjcxMWZiMTkwZDA4NzA2MjQxN2FhOA"
                     :headers    {"Content-Type"  "application/json"
                                  "Accept-Version" "1.8"
                                  "Authorization" "Bearer NDk1MzI4MWUwNTRjZDkxMWQxNDU5ZTU1NjAyZTRlZmE2MjEwNTE2ZTY4ZjcxMWZiMTkwZDA4NzA2MjQxN2FhOA"}
                     }
            {:keys [status  body error] :as resp}  @(http/get (str "https://web.mention.net/api/accounts/772921_qxho63ya1zk8sk0840sg8cwok84gkogoskw808k4ssosks844/alerts/"id) options)]

        (if (= status 200)
          (do
            (println "fetch alert success" status body)
            "fetch-success" )
          (do
            (println "!fetch alert failed" error resp)
            "fetch-failed"))))



    (defn mention
      [id men_id]
      (println "Create Alert")
      (let [options {:timeout     5000
                     :oauth-token "NDk1MzI4MWUwNTRjZDkxMWQxNDU5ZTU1NjAyZTRlZmE2MjEwNTE2ZTY4ZjcxMWZiMTkwZDA4NzA2MjQxN2FhOA"
                     :headers    {"Content-Type"  "application/json"
                                  "Accept-Version" "1.8"
                                  "Authorization" "Bearer NDk1MzI4MWUwNTRjZDkxMWQxNDU5ZTU1NjAyZTRlZmE2MjEwNTE2ZTY4ZjcxMWZiMTkwZDA4NzA2MjQxN2FhOA"}
                     }
            {:keys [status  body error] :as resp}  @(http/get (str "https://api.mention.net/api/accounts/772921_qxho63ya1zk8sk0840sg8cwok84gkogoskw808k4ssosks844/alerts/"id "/mentions/"men_id) options)]

        (if (= status 200)
          (do
            (println "fetch alert success" status body)
            "fetch-success" )
          (do
            (println "!fetch alert failed" error resp)
            "fetch-failed"))))
    )