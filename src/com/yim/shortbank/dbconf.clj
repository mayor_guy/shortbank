(ns com.yim.shortbank.dbconf
  (:use [korma.db]
        [korma.core :exclude [update]]
        [com.yim.shortbank.config]
        [clojure.tools.logging])
  (:import (java.util Properties Timer TimerTask)
           (org.joda.time DateTime)))

(System/setProperties
  (doto (Properties. (System/getProperties))
    (.put "com.mchange.v2.log.MLog" "com.mchange.v2.log.FallbackMLog")
    (.put "com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL" "OFF")))


(declare session-exists?)
(def call-denoms (atom nil))

(defn initialize-db [args]
  (defdb db (postgres args)))



(defentity tbl_charge_denoms
           (table :tbl_charge_denoms))


(defentity tbl_charge_requests_new
           (table :tbl_charge_requests_new))




(defn insert-requests
  "Inserts into tbl_charge_requests_new, returns done when completed
 or returns failed"
  [id time sub amount_requested subscription_fk]
  (debug "Inserting |" id time sub amount_requested subscription_fk)
  (try
    (let [insert (insert tbl_charge_requests_new
                         (values {:request_id       id
                                  :event_time       time
                                  :subscriber_no    sub
                                  :charge_status    -1
                                  :kobo_charged    amount_requested
                                  :error_message    ""
                                  :flag_done        (boolean false)
                                  :flag_Reconciled  (boolean false)
                                  :subscription_fk subscription_fk}))]
      (debug "Inserted |" insert)
      "done")
    (catch Exception e
      (throw (Exception. (format "!inserting fails -> [%s]." (.getMessage e)))))))





(defn call-get-denoms
  "get denoms"
  [& args]
  (try
    (let [call-denoms (atom nil)
          get-d (select tbl_charge_denoms
                        (order :kobo_value :desc))]
      (doseq [v get-d]
        (swap! call-denoms conj v))
      (def denoms @call-denoms)
      (info "Initializing Denoms "denoms))
    (catch Exception e
      (do
        (error (.getMessage e))
        (throw (Exception. (format "Error Initializing Database Denoms [%s]." (.getMessage e))))))))


(def get-bal-in-rate-code
  "Gets corresponding value deductable by PSA"
  (fn [val]
    (let [den (reduce conj () (map :kobo_value denoms))]
      (first (filter #(>= val %) den)))))


(defn get-denoms [code]
  (doseq [n denoms]
    (if (= (n :kobo_value) code)
      (def denom (n :rate_code))))denom)



(let [states     {:initial 0 :new-pin 1 :confirm-pin 2 :menu 3 :subscription-menu 4}
      keys       (reduce conj #{} (map (fn [[k v]] k) states))
      values     (reduce conj #{} (map (fn [[k v]] v) states))
      rev-states (reduce (fn [res [k v]] (assoc res v k)) {} states)]
  (defn state->int [state]
    (if (integer? state)
      (values state)
      (states state)))
  (defn state<-int [state]
    (when (values state)
      (rev-states state))))

(defn get-session-data [session-id subscriber]
  (let [query (format (str "select request_id, subscriber_fk, session_state"
                           " from tbl_sessions where session_id = '%s' and time_expiry > now()") session-id)
        [set1] (exec-raw [query]:results)
        {:keys [request_id subscriber_fk session_state]}set1]
    (infof "Session check (request-id=%s,subscriber=%s,state=%s)"  request_id subscriber_fk session_state)
    ;; Ensure that the session actually exists.
    (when-not subscriber_fk
      (throw (Exception. (format "sessionNotFound(%s,sub=%s)" session-id subscriber))))
    ;; Validate session data.
    (when-not (= subscriber subscriber_fk)
      (throw (Exception. (format "sessionMismatch(%s,expected=%s,found=%s)"
                                 session-id subscriber subscriber_fk))))
    ;; ---
    {:session-id session-id :subscriber subscriber_fk :request-id request_id
     :state (state<-int (biginteger session_state))}))


(defn start-new-session
  ([session-data reuse-sessions?]
   "Start a new session. Return the result of the session start
operation."
   (let [%insert
         (fn [initial-state]
           (let [{:keys [session-id request-id subscriber]} session-data]
             ;; Ensure that this is actually a new session.
             (when (session-exists? session-id)
               (throw (Exception. (format "dupSessionID(%s,msisdn=%s)" session-id subscriber))))
             ;; ---
             (let [query (format (str "insert into tbl_sessions"
                                      " (session_id, request_id, subscriber_fk, session_state)"
                                      " values ('%s', '%s', '%s', '%s')") session-id request-id subscriber
                                      (state->int initial-state))
                   rows-affected (exec-raw [query])]
               true)))]
     (if reuse-sessions?
       ;; Yes, we can reuse sessions. Do we have an unexpired
       ;; session for this subscriber?  If yes, borrow its state
       ;; and use it as the new session entry's state. Otherwise,
       ;; use the state passed at invocation.
       (let [query (format (str "select session_id, session_state from tbl_sessions"
                                " where subscriber_fk = %s"
                                " and time_expiry > now()") (session-data :subscriber))
             [session-state] (exec-raw [query]:results)
             prev-session (:session_id session-state)
             prev-state (:session_state session-state)]
         (if prev-session
           (%insert prev-state)
           (%insert (session-data :state))))
       ;; ---
       (%insert (session-data :state)))))
  ([session-data]
   (start-new-session session-data false)))


(defn decisions [subscriber]
  (let [query (str "select profile from tbl_decisioning
    where subscriber_fk = "subscriber)]
    (exec-raw [query]:results)))


(defn check-unreconciled-txn [subscriber]
  (let [query (str "select amount_requested "
                   " from tbl_log_indebit_new where subscriber_fk = " subscriber " and debit_status in (-1,2)")
        [result] (exec-raw [query]:results)]
    (if (empty? result)  0 (biginteger result))))




(defn session-exists?
  ([session-id]
   (let [query (str "select subscriber_fk"
                    " from tbl_sessions where session_id = '" session-id
                    "' and time_expiry > now()")
         result (exec-raw [query]:results)]
     (if (empty? result)
       false
       true)))
  ([session-id subscriber]
    (let [query (str "select subscriber_fk"
                     " from tbl_sessions where session_id = " session-id
                     "::text and time_expiry > now()")
          result (exec-raw [query]:results)
          %subscriber (:subscriber_fk result)]

    (when result
       (if (= subscriber %subscriber)
         true
         (throw (Exception. (format "sessionMismatch(%s,expected=%s,found=%s)"
                                           session-id subscriber %subscriber))))))))



;; ---

(defn- get-target-table [action]
  (let [now (DateTime.)
        y (int (.getYear now))
        m (int (.getMonthOfYear now))]
    (when (= action :lending)
      (str  "tbl_transfer_req_" (if (< m 10) (str y"0"m) (str y m))))))


(defn new-loan-request [request-id subscriber amount channel]
  (exec-raw [(format (str "insert into " (get-target-table :lending)
                          " (request_id, subscriber_fk, amount_requested, channel)"
                          " values (%d, %d, %d, %d, %d, '%s')")
                     request-id subscriber amount channel)]:results))

(let [status-flags {:vtu    2r00000100  ; Sent to VTU.
                    }]
  (defn set-loan-request-status [request-id field succeeded? error]
    (let [error (when-not succeeded?
                  (or error :unknown-error))]
      (exec-raw [(format (str "update " (get-target-table :lending) " set"
                              " error_message = nullif('%s', ''),"
                              " status_flags = status_flags | %d"
                              " where loan_request_id = %d")
                         error
                         ;(utils/escape-string (if error (utils/sqlify-keyword error) ""))
                         (status-flags field)
                         request-id)]:results))))
;; -------




(defn- create-partitions []
  (let [now (DateTime.)
        y (.getYear now)
        m (.getMonthOfYear now)
        d (.getDayOfMonth now)
        query (format "select * from proc_create_partitions(%d, %d, %d)" y m m)]
    ;; Create partitions for this month.
    (exec-raw [query]:results)
    (when (>= d 25)
      ;; On the 25th of every month, create partitions for
      ;; succeeding month.
      (let [[y m] (if (= m 12)
                    [(inc y) 1]
                    [y (inc m)])
            query (format "select * from proc_create_partitions(%d, %d, %d)" y m m)]
        (exec-raw [query]:results)))))


(defn start-partition-creator []
  (create-partitions)
  (let [ms-per-day (long (* 24 60 60 1000))]
    (doto (Timer. "partition-creator" true)
      (.scheduleAtFixedRate (proxy [TimerTask] []
                              (run [] (create-partitions)))
                            ms-per-day ms-per-day))))