(ns com.yim.shortbank.config
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use [clojure.tools.logging])
  (:import (java.io File PushbackReader)))

;;; commense processing the config file

(def app-configs (atom {}))

(defn- config-file
  []
  (let [
        ;result (System/getProperty "com.yim.shortbank.file")
        result "F:\\env\\config-transfer.md"
        ]
    (if (and result (-> result File. .isFile))
      result
      (do (fatal (format "serverConfig(%s) = nil" result))
          (throw (Exception. (format "Server configuration file (%s) not found." result)))))))

(defn load-config [config]
  (let [configs (with-open [rdr (io/reader config :encoding "UTF-8")]
                  (doall (line-seq rdr)))
        configs (remove #(or (.startsWith % "#")
                             (= 0 (count %))) configs)]
    (into {}
          (map (fn [line]
                 (let [[param value] (str/split line #"=" 2)]
                   (if param
                     {(keyword (str/trim param))
                      (str/trim value)}
                     {})))
               configs))))

(defn submsisdn [sub]
  (let [sub (biginteger sub)]
  (if (= (count sub) 13)
    (subs sub 3)
    (if (= (count sub) 11)
      (subs sub 1)
      (if (= (count sub) 10)
        sub)))))

(defn initialise-app-server-config []
  (info "Initializing configurations..")
  (reset! app-configs (load-config (config-file)))
  (infof "configs=%s" @app-configs)

  (def app-port (let [res (:app-port @app-configs)
                      ]
                  (if (empty? res)
                    (do (fatal (format "APP port (%s) = nil" res))
                        (throw (Exception. (format "App port (%s) not found." res))))
                    (biginteger res))))
  ;;; path to the URL to charge or get balance
  (def in-url (let [res (:in-url @app-configs)
                    ]
                (if (empty? res)
                  (do (fatal (format "Charging Address (%s) = nil" res))
                      (throw (Exception. (format "Charging Address (%s) not found." res))))

                  (str/trim res))))


  ;;Database Port
  (def dbport (let [res (:dbport @app-configs)]
                (if (empty? res)
                  (do (fatalf "Database Port (%s) = nil" res)
                      (throw (Exception. (format "Database Port (%s) not found." res))))

                  (str/trim res))))

  ;;Host IP
  (def dbhost (let [res (:dbhost @app-configs)]
                (if (empty? res)
                  (do (fatalf "Database Host (%s) = nil" res)
                      (throw (Exception. (format "Database Host (%s) not found." res))))

                  (str/trim res))))

  ;;database password
  (def dbpassword (let [res (:dbpassword @app-configs)]
                    (if (empty? res)
                      (do (fatalf "Database Password (%s) = nil" res)
                          (throw (Exception. (format "Database Password (%s) not found." res))))

                      (str/trim res))))

  ;;user
  (def dbuser (let [res (:dbuser @app-configs)]
                (if (empty? res)
                  (do (fatalf "Database User (%s) = nil" res)
                      (throw (Exception. (format "Database User (%s) not found." res))))

                  (str/trim res))))


  ;; datebase name
  (def dbname (let [res (:dbname @app-configs)]
                (if (empty? res)
                  (do (fatalf "Database Name (%s) = nil" res)
                      (throw (Exception. (format "Database Name (%s) not found." res))))

                  (str/trim res))))



  ;;; section for socket pool configuration
  (def read-timeout (or (:read-timeout @app-configs) 5000))
  (def connect-timeout (or (:connect-timeout @app-configs) 5000))


  ;;; path to the URL to charge or get balance
  (def sms-url (let [res (:sms-url @app-configs)]
                 (if (empty? res)
                   (do (fatal (format "SMS Address (%s) = nil" res))
                       (throw (Exception. (format "SMS Address (%s) not found." res))))

                   (str/trim res))))

  ;;message header or product name
  (def sms-header (let [res (:sms-header @app-configs)]
                    (if (empty? res)
                      (do (fatalf "SMS header Name (%s) = nil" res)
                          (throw (Exception. (format "SMS header Name (%s) not found." res))))

                      (str/trim res))))

  (def ussd-subscription-top-menu (:ussd-subscription-top-menu @app-configs))
  (def msg-decisioning-error (:msg-decisioning-error @app-configs))
  (def msg-postpaid (:msg-postpaid @app-configs))
  (def msg-has-queued-request (:msg-has-queued-request @app-configs))
  (def msg-retry-menu (:msg-retry-menu @app-configs))
  (def msg-qualification-failure (:msg-qualification-failure @app-configs))
  (def msg-low-balance (:msg-low-balance @app-configs))
  (def msg-error-menu (:msg-error-menu @app-configs))
  (def msg-service-unavailable (:msg-service-unavailable @app-configs))
  (def msg-ex-error (:msg-ex-error @app-configs))
  (def bank-api-url (:bank-api-url @app-configs))
  (def bank-balance-url (:bank-balance-url  @app-configs))
  (def bank-public-key (:bank-public-key @app-configs))
  (def app-bank-timeout (:app-bank-timeout @app-configs))
  (def msg-bank-unavailable (:msg-bank-unavailable @app-configs))


  (defn- byte-transform
    "Used to encode and decode strings.  Returns nil when an exception
    was raised."
    [direction-fn string]
    (try
      (apply str (map char (direction-fn (.getBytes string))))
      (catch Exception _)))




  ;;; section for XML requests
  (def query-balance-xml (let [res (:query-balance-xml @app-configs)]
                           (if (and res (-> ^String res File. .isFile))
                             (slurp res)
                             (do (fatal (format "noQueryBalanceXML(%s) = nil" res))
                                 (throw (Exception. (format "XML query balance file response (%s) not found." res)))))))
  (def adjust-balance-xml (let [res (:adjust-balance-xml @app-configs)]
                            (if (and res (-> ^String res File. .isFile))
                              (slurp res)
                              (do (fatal (format "noAdjustBalanceXML(%s) = nil" res))
                                  (throw (Exception. (format "XML AjustBalance file response (%s) not found." res)))))))


  (def alert-rest-duration (or (:alert-rest-duration @app-configs) 5000)))




