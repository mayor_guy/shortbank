(ns com.yim.shortbank.rpc
  (:require [clojure.string :as str]
            [clojure.walk :as walk]
            [com.yim.shortbank.dbconf :as db]
            [com.yim.shortbank.callsms :as sms]
            [clojure.data.json :as json])
  (:use [clojure.tools.logging]
        [com.yim.shortbank.config])
  (:import (java.text SimpleDateFormat)
           (java.util Date)
           (java.sql Timestamp)))



(defn make-response [status action sub msg]
  {:status  status
   :headers {"Content-Type" "application/json"}
   :body    (json/write-str {:message msg, :sub sub, :state (cond (= action :terminate) "S"
                                                                  (= action :continue) "C"
                                                                  :else "?")})})

(defmacro client-error []
  `{:status 400
    :headers {"Content-Type" "text/json"}
    :body "Bad request"})


(defn process-url-encoded
  "Receives incoming GET HTTP requests and
  returns the appropriate HTTP response"
  [req]
  (let [headers (:content-type req)
        input (or (:query-string req) (:input-string req)) ]
    (when (= nil headers)
      (do
        (let [input-vec (apply hash-map (str/split input #"[&=]"))]
          (info "Input "(walk/keywordize-keys input-vec))
          (walk/keywordize-keys input-vec))))))



(defn request-id
  "Makes a 19 digit session identifer"
  []
  (let [component-id "33"
        timestamp (System/currentTimeMillis)
        rand (format "%04d" (rand-int 9999))
        request-id (str component-id timestamp rand)]
    (biginteger request-id)))

(defn increment-counter [the-atom value]
  (swap! the-atom (fn [current_atom_values new_value]
                    (let [c (current_atom_values new_value)]
                      (if c
                        (assoc current_atom_values new_value (inc c))
                        (assoc current_atom_values new_value 1))))
         value))

(defmacro with-funcall-logging [[function-name & args] & body]
  `(try
     (do (infof "%s(args=%s)" ~function-name [~@args])
         ~@body)
     (catch Exception e#
       (errorf "!%s(args=%s) -> %s" ~function-name [~@args]
               (.getMessage e#))
       (throw e#))))

(defmacro with-time-logging [[arg1 & args] & body]
  ;; Validate inputs.
  (cond (or (nil? arg1)
            (and (keyword? arg1) (empty? args)))
        (throw (IllegalArgumentException. "Function name expected.")))
  (when (and (keyword? arg1)
             (not (= arg1 :verbose)))
    (throw (IllegalArgumentException. ":verbose directive or function name expected.")))
  ;; ---
  (let [verbose? (and (keyword? arg1) (= arg1 :verbose))
        function-name (if verbose? (first args) arg1)
        args (if verbose? (rest args) args)]
    (if verbose?
      `(do (infof "%s(args=%s)" ~function-name [~@args])
           (let [start# (System/currentTimeMillis)]
             (try
               (do ~@body)
               (finally
                 (let [end# (- (System/currentTimeMillis) start#)]
                   (infof "callProf|%s|%s|%s" ~function-name end# [~@args]))))))
      `(let [start# (System/currentTimeMillis)]
         (try
           (do ~@body)
           (finally
             (let [end# (- (System/currentTimeMillis) start#)]
               (infof "callProf|%s|%s|%s" ~function-name end# [~@args]))))))))

(defn check [s]
  (when (seq s)
    (if (str/blank? (first s))
      "empty"
      (do
        (recur (rest s))))))

;(defn rpl [strg] (str/replace strg #"[\{\(\)\}]" ""))

