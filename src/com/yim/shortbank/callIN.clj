(ns com.yim.shortbank.callIN
  (:use [clojure.xml :only [parse]])
  (:use [clojure.tools.logging])
  (:require [clojure.string :as str]
            [com.yim.shortbank.dbconf :as db]
            [org.httpkit.client :as http :exclude get]
            [com.yim.shortbank.config :as conf]
            [com.yim.shortbank.rpc :as rpc])
  (:import (java.io ByteArrayInputStream)
           (java.net ConnectException SocketTimeoutException)))


(defn call-getbal [sid msisdn args]
  (let [fullUrl  conf/in-url
        current-time (quot (System/currentTimeMillis) 1000)
        request (str/trim (format conf/query-balance-xml sid current-time msisdn))
        options  {:timeout    conf/connect-timeout
                  ;:basic-auth conf/auth
                  :body       request
                  :headers    {"Content-Type" "application/xml"}}
        {:keys [status  body error] :as resp}  @(http/post fullUrl options)]
    (infof "%s(args=%s)" "getBalRequest" (str "(msisdn="msisdn ",url="fullUrl ",request="(str/replace request #"\s+" "")))
    (let [[success? result] (if error
                                (do
                                  (errorf "Failed, exception |%s" error )
                                  [false error])
                                (do
                                  (infof "Http Status [%s|%s|%s|%s]" status sid msisdn (str/replace body #"\s+" ""))
                                  (let [body-parser (parse
                                                      (ByteArrayInputStream. (.getBytes (str/trim body))))
                                        {soap-header :tag} body-parser
                                        {[{[{txnID :content}  {retry :content}
                                            {resptag :tag responsecode :content}] :content}] :content} body-parser]
                                    (cond (= (str soap-header) ":ucap:ChargeResponse")
                                          ;;get balance xml format
                                          (cond
                                            ;; prepaid msisdn normal code is Zero
                                            (= (responsecode 0) "0") (let [{[{[{txnID :content}] :content}] :content} body-parser
                                                                           {[{[{c1 :content} {t1 :tag, c2 :content} {t2 :tag, c3 :content}] :content}
                                                                             {[{c2-1 :content} {t1t :tag, c2-2 :content} {t2t :tag, c2-3 :content}
                                                                               {t3t :tag, c3-3 :content}] :content}] :content} body-parser]
                                                                       (if (= (str t3t) ":ucap:AmountValue")
                                                                         (let [MAbal (c3-3 0) ;val (db/get-bal-in-rate-code (Integer. ))
                                                                               ]
                                                                           [true  MAbal])

                                                                         (do
                                                                           (error "Incorrect XML tag |" t3t "|" sid "|" msisdn)
                                                                           [false "Incorrect XML tag"])))
                                            ;; postpaid msisdn response code is 99

                                            (= (responsecode 0) "99") (do
                                                                        (infof "getBal|HYBRID [responseCode=%s,sid=%s,msisdn=%s]" (responsecode 0)  sid msisdn)
                                                                        [false "hybrid"])
                                            (= (responsecode 0) "24") (do
                                                                        (errorf "!getBal[msisdn:%s|DuplicateSessionID=%s,responseCode=%s]" msisdn sid (responsecode 0) )
                                                                        [false "duplicateSessionId"])
                                            :else (do
                                                    (infof "getBal|Unknown responsecode=%s,sid=%s,msisdn=%s"(responsecode 0) sid msisdn)
                                                    [false "Unknown response code"]))

                                          (= (str soap-header) ":ErrorType") (do
                                                                               (let [{[{stat :content}
                                                                                       {desc :content}]:content}body-parser]
                                                                                 (errorf "!getBal|msisdn:%s|ErrorCode:%s|Desc:%s]" msisdn (stat 0) (desc 0))
                                                                                 [false "error"]))


                                          :else (do
                                                  (error "!getBal|Incorrect XML Header [%s|%s|%s|%s]" soap-header sid msisdn)
                                                  [false "Incorrect XML Header"])))))]
      [success? result])))





(defn getMABal [request-id subscriber & args]
  (rpc/with-time-logging ["callGetMABal" request-id subscriber args]
                         ;;(.toURI #^java.net.URL (java.net.URL. (format config/psa-url request-id))) request (psa-header)
                         (call-getbal request-id subscriber args)))





(comment defn call-debit
  "Calls PSA and return failed or success with corresponding
  status code"
  [fullUrl psa-sid request amt as_connect_timeout msisdn]
  (info "callPSA | Debit Request |" fullUrl "|" (str/trim (str/join "\n" (str/split-lines request))))
  (let [debitstart (System/currentTimeMillis)
        options  {:timeout (Integer. as_connect_timeout)
                  ;:basic-auth conf/auth
                  :body request
                  :headers {"Content-Type" "application/xml"}}
        {:keys [status  body error] :as resp}  @(http/post fullUrl options)]
    (if error
      (do
        (errorf "Failed, exception %s" error)
        "failed")
      (do
        (infof "Http Status %s|%s|%s" status msisdn (str/trim (str/join "\n" (str/split-lines body))))
        (let [body-parser (parse
                            (ByteArrayInputStream. (.getBytes (str/trim body))))
              {soap-header :tag} body-parser]
          (debug "Parsed response" body-parser)
          (debug "Response header" soap-header)

          (cond (= (str soap-header) ":ucap:ChargeResponse")
                (let [{[{[{txnID :content} {retry :content} {responseCode :content}] :content}] :content} body-parser
                      debit-status (Integer. (responseCode 0))
                      txnID (biginteger (txnID 0))]
                  (do
                    (infof "callProf|debitMA|%s|[%s %s %s]" (- (System/currentTimeMillis) debitstart) txnID msisdn fullUrl)
                    (if (= (str debit-status) "0")
                      ;; insert into DB here and run proc
                      (when (and (db/update-charge-requests txnID amt (Integer. debit-status)) true)
                        (infof "Success [%s|%s|%s|%s]" txnID debit-status msisdn amt)
                        (str "200:success"))
                      (when (and (db/update-charge-requests txnID amt debit-status) true)
                        (infof "Failed [%s|%s|%s|%s]" txnID debit-status msisdn amt)
                        (str "200:failed")))))

                (= (str soap-header) ":ErrorType") (let [update-rec (db/update-charge-requests psa-sid amt 99)
                                                         {[{ErrorCode :content} {errorDesc :content}] :content}body-parser]
                                                     (errorf "!callapp|msisdn:%s|ErrorCode:%s|Desc %s" msisdn (ErrorCode 0) (errorDesc 0))
                                                     (str "200:failed:"))
                :else (when (and (db/update-charge-requests psa-sid amt 99) true)
                        (errorf "!callapp|msisdn:%s|Soap-header not correct:%s" msisdn soap-header)
                        (str "200:failed:"))))))))



(comment defn callPSA [request-id subscriber & args]
  (rpc/with-time-logging ["callPSA" request-id subscriber args]
                         ;;(.toURI #^java.net.URL (java.net.URL. (format config/psa-url request-id))) request (psa-header)
                         (call-debit request-id subscriber args)))